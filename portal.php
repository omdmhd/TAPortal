<?php 
	require_once("header.php");
?>
  <div class="jumbotron">
    <h1>به صحفه شخصی خود در پرتال دانشجویی خوش آمدید</h1>
    <p></p>
  </div>
  <div>
      
       <div class="input-group">
            <span class="input-group-addon">جست و جوی درس</span>
            <input id="search" type="text" class="form-control" name="search" placeholder="اسم درس" oninput="changer()">
        </div>
      <div class="container" id="searchtable"></div>

  </div>


<script>
    function changer(){
        formdata = new FormData();
        var text = document.getElementById("search").value;
        formdata.append("job","livecoursesearch")
        formdata.append("text",text)
        $.ajax({
            url: "function.php", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false, 
            cache:false,
            success: function(data)   // A function to be called if request succeeds
                {
                    document.getElementById("searchtable").innerHTML=data;
                }
        });
    }
    function addthiscourse(crsid){
        formdata = new FormData();
        formdata.append("job","addthiscourse")
        formdata.append("id",crsid)
        $.ajax({
            url: "function.php", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: formdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false, 
            cache:false,
            success: function(data)   // A function to be called if request succeeds
                {
                    alert(data);
                }
        });
    }

</script>
<?php
	require_once("footer.php");
	
?>
