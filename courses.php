<?php 
	require_once("header.php");
?>
<br><br>
<div class="container">
  <h2>دروسی که توسط حل تمرین ها برای شما ثبت شده است</h2>
  <table class="table">
    <thead>
      <tr>
        <th>نام درس</th>
        <th>نام استاد حل تمرین</th>
        <th>لینک تمرین های این درس</th>
      </tr>
    </thead>
    <tbody>
      <?php
        
        require_once("model/courses.php");
        $crs = new Courses();
        $stu = $_SESSION['stuid'];
        $crses = $crs->getStudentCourse($stu);
        foreach( $crses as $crsss) {
            $crsdet = $crs->getCourseDetail($stu,$crsss['course_code']); 
            echo "
                <tr>
                    <td>".$crsdet[0]["name"]."</td>
                    <td>".$crsdet[0]["taname"]."</td>";
            if($crsdet[0]["confirm"] == 1 ){
                   echo " <td><a href='course.php?id=".$crsdet[0]["id"]."' class='btn  btn-block btn-default' role='button'>مراجعه به صحفه درس</a></td>";
            }else{
                   echo " <td><a href='#' class='btn  btn-block btn-default disabled' role='button'>هنوز این درس برای شما تایید نشده</a></td>";
            }
            echo"
                </tr>
                ";
        }
        ?>
    </tbody>
  </table>
</div>


<?php
	require_once("footer.php");
	
?>
