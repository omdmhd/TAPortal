<?php
	require_once("TAheader.php");
?>
<?php
	require_once("model/student.php");

	$id = "";
	$password = "";
	$name = "";
	$err = "";
	if(count($_POST) != 0 ){
		if( $_POST['id'] != null && $_POST['name'] != null && $_POST['password'] != null ){
			
			$id = $_POST['id'];
			$password = $_POST['password'];
			$name = $_POST['name'];
			$email = "";
			$std = new Student();
			$out = $std->add_new_student($id,$name,$password,$email);
			if($out == false){
				$err = 'این دانشجو قبلا اضافه شده است';
			}else{
				$id = "";
				$password = "";
				$name = "";
				$err = "";			
				echo "<script>alert('با موفقیت اضافه شد');</script>";
			}
			
		}else{
			echo "<br/><br/><br/><br/><br/><br/><br/><br/>";
			die("Forbidden Operation");
		}
	}
	
	
?>

<div class="container-fluid" style="margin-top:8%;">
	<div class="row" style="margin-bottom:3%">
		<div class="col-md-12" style="text-align:center;">
			<h1>اضافه کردن دانشجو</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6" style="width:2% auto;">


			<form method="POST" action="TAAddStd.php">
			  <label class="err" ><?php echo $err;?></label>
			  <div class="form-group">
				<label for="exampleInputEmail1">شماره ی دانشجویی</label>
				<input type="text"  name="id" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="شماره دانشجویی">
			  </div>
			  <div class="form-group">
				<label for="exampleInputEmail1">نام و نام خانوادگی</label>
				<input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="نام و نام خانوادگی">
			  </div>
			  <div class="form-group">
				<label for="exampleInputPassword1">کلمه ی عبور</label>
				<input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="کلمه ی عبور">
			  </div>
	

			  <button type="submit" class="btn btn-primary">ثبت</button>
			</form>
		
		
		</div>

		<div class="col-md-3"></div>
	
	</div>
	
</div>


<?php
	require_once("TAfooter.php");
?>
