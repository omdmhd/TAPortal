<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	require_once("db/database.php");
	class Exercise{
		protected $cur;
		protected $connection_status = true;
		function __construct(){
			$this->cur = new Db();
		}
		function addNewExercise($name,$description,$start_date,$end_date,$course_code,$file_name,$expire_message=''){
			$name = $this->cur->quote($name);
			$description = $this->cur->quote($description);
			$start_date = $this->cur->quote($start_date);
			$end_date = $this->cur->quote($end_date);
			$course_code = $this->cur->quote($course_code);
			$file_name = $this->cur->quote($file_name);
			$expire_message = $this->cur->quote($expire_message);
			$sql = "INSERT INTO exercises(name,description,start_date,end_date,course_code,file_name,expire_message) values(". $name . ',' . $description . ',' . $start_date . ',' . $end_date . ',' . $course_code . ',' . $file_name . ',' . $expire_message . ")";
			echo $sql;
			$result = $this->cur->query($sql);
			echo $this->cur->error();
			return true;				
		}

		function getStudentCourse(){
			if($this->connection_status == false) return false;
			$sql = "SELECT * from courses where ";
			$result = $this->cur->select($sql);
			echo $this->cur->error();

			return $result;
		}
		function getTaCourse($TA){

			if($this->connection_status == false) return false;
			$TA = $this->cur->quote($TA);
			$sql = 'select course_code,name,TA from TA_courses inner join courses on TA_courses.course_code = courses.id where TA = '.$TA;
			$result = $this->cur->select($sql);
			return $result;

		}
		function checkCourseExis($TA,$course_code){
			if($this->connection_status == false) return false;
			$TA = $this->cur->quote($TA);
			$course_code = $this->cur->quote($course_code);
			$sql = 'select course_code,name,TA from TA_courses inner join courses on TA_courses.course_code = courses.id where TA = '.$TA .' and course_code = ' .$course_code;
			$result = $this->cur->select($sql);
			if(count($result) == 0){
				return false;
			}
			return true;
			
		}
        function ShowExercises($crsid){
            $sql = "SELECT * from exercises where course_code = '".$crsid."' ;";
            $result = $this->cur->select($sql);
            echo $this->cur->error();
			return $result;
        }
        function CheckExerSolve($stuid,$exerid){
            if($this->connection_status == false) return false;
			$sql = "SELECT id,grade from  exercise_answers where student= ". '"' .$stuid .'"'. ' and exercise = ' .'"' .$exerid . '";';
			$result = $this->cur->select($sql);
			if(count($result) == 1){
                    $res = array(true,$result[0]["grade"]);
					return $res;
			}
			else{
                $res = array(false);
				return $res;
			}
        }
	}
	

	
	
?>
