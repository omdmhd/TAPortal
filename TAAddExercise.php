<?php
	
	function jalali_to_gregorian($jy,$jm,$jd,$mod=''){
	 if($jy>979){
	  $gy=1600;
	  $jy-=979;
	 }else{
	  $gy=621;
	 }
	 $days=(365*$jy) +(((int)($jy/33))*8) +((int)((($jy%33)+3)/4)) +78 +$jd +(($jm<7)?($jm-1)*31:(($jm-7)*30)+186);
	 $gy+=400*((int)($days/146097));
	 $days%=146097;
	 if($days > 36524){
	  $gy+=100*((int)(--$days/36524));
	  $days%=36524;
	  if($days >= 365)$days++;
	 }
	 $gy+=4*((int)($days/1461));
	 $days%=1461;
	 if($days > 365){
	  $gy+=(int)(($days-1)/365);
	  $days=($days-1)%365;
	 }
	 $gd=$days+1;
	 foreach(array(0,31,(($gy%4==0 and $gy%100!=0) or ($gy%400==0))?29:28 ,31,30,31,30,31,31,30,31,30,31) as $gm=>$v){
	  if($gd<=$v)break;
	  $gd-=$v;
	 }
	 return($mod=='')?array($gy,$gm,$gd):$gy.$mod.$gm.$mod.$gd; 
	}

	require_once("TAheader.php");
	require_once("model/courses.php");
	require_once("model/exercise.php");
	$title = '';
	$startTime = '';
	$endTime = '';
	$description ='';
	$err = '';
	$success  = '';
	if( count($_GET) == 0 ){
		echo "<br/><br/><br/><br/><br/><br/>";
		die("INVALID PARAMETERS");
	}else{
		if( isset($_GET['courseCode']) ){
			$course = new Courses();
			$username = $_SESSION['username'];
			$course_code = htmlspecialchars($_GET['courseCode']);
			$check = $course->checkCourseExis($username,$course_code);
			if($check == false){
				echo "<br/><br/><br/><br/><br/><br/>";
				die("Invalid Course Code");
			}else{
				if($_SERVER['REQUEST_METHOD'] == 'POST'){
					if( isset($_POST['title']) && isset($_POST['startTime']) && isset($_POST['endTime']) && isset($_POST['description']) && isset($_FILES['file']) ){
						$title = htmlspecialchars($_POST['title']);
						$startTime = htmlspecialchars($_POST['startTime']);
						$endTime = htmlspecialchars($_POST['endTime']);
						$description = htmlspecialchars($_POST['description']);

					if( $title!= '' && $startTime && $endTime != '' && $description != '' ) {
						//should be checked for Max Size for text fields

						if($_FILES['file']['size']  > 3300000){
							$err = 'حداکثر حجم مجاز ۳ مگابایت است';
						}else{
							$_prefix =  uniqid()."_";
							$target_dir = "files/ifYouCanFindThisLocationasdjj2wqek/";
							$file_name = $_prefix.$_FILES["file"]["name"];
							$target_file = $target_dir . basename($file_name);
							$FileType = pathinfo($target_file,PATHINFO_EXTENSION);
							if($FileType == 'zip'){
								
								$out = move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
								echo "HERE";
								print_r($out);
								if( $out ){
									/*
									$start_day = substr($startTime,6,2);
									$start_month = substr($startTime,4,2);
									$start_year = substr($startTime,0,4);
									
									$startT = jalali_to_gregorian($start_year,$start_month,$start_day);
		
									$end_day = substr($endTime,6,2);
									$end_month = substr($endTime,4,2);
									$end_year = substr($endTime,0,4);
									$endT = jalali_to_gregorian($end_year,$end_month,$end_day);
									*/

									
									$Exe = new Exercise();
									$Exe->addNewExercise($title,$description,$startTime,$endTime,$course_code,$file_name,'');
									$title = '';
									$startTime = '';
									$endTime = '';
									$description ='';
									$err = '';
									$success = "عملیات با موفقیت انجام شد";
								}else{
									$err = 'خطا در آپلود فایل';
										$success  = '';
								}
							}else{
								$err = 'لطفا تمرین های خود را به صورت فایل zip ارسال نمایید ';
									$success  = '';
							}
						}
						
					}else{
							$err = 'لطفا فرم را تکمیل کنید .';
								$success  = '';
					}


					}else{
						$err = 'درخواست اشتباه';
							$success  = '';
					}
					
				}else{
					echo "POST";

				}
			} 
		}else{
			echo "<br/><br/><br/><br/><br/><br/>";
			die("INVALID PARAMETERS");			
		}
	}
?>

<div class="container-fluid" style="margin-top:8%;">
	<div class="row" style="margin-bottom:3%">
		<div class="col-md-12" style="text-align:center;">
			<h1>اضافه کردن تمرین</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6" style="width:2% auto;">


		<form method="POST" enctype="multipart/form-data">
			  <label class="success"><?php echo $success ?></label>
			  <label class="err" ><?php echo $err;?></label>
			  <div class="form-group">
				<label for="exampleInputEmail1">عنوان تمرین</label>
				<input type="" class="form-control" name="title" id="exampleInputEmail1" value="<?php echo $title;?>" aria-describedby="emailHelp" placeholder="عنوان تمرین">
			  </div>
			  <div class="form-group">
				<label for="exampleInputEmail1">زمان شروع </label>
				<input type="" class="form-control" name="startTime" id="exampleInputEmail1" value="<?php echo $startTime;?>" aria-describedby="emailHelp" placeholder="زمان شروع 12121390">
			  </div>

			  <div class="form-group">
				<label for="exampleInputEmail1">زمان پایان </label>
				<input type="" class="form-control" name="endTime" value="<?php echo $endTime;?>" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="زمان پایان 01011391">
			  </div>



			  <div class="form-group">
				<label for="exampleTextarea">توضیحات تمرین</label>
				<textarea class="form-control" name="description" id="exampleTextarea" rows="3"><?php echo $description;?></textarea>
			  </div>
			 <div class="form-group">
				<label for="exampleInputFile">فایل تمرین</label>
				<input type="file" name="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
				<small id="fileHelp" class="form-text text-muted">.</small>
			  </div>
			  <button type="submit" class="btn btn-primary">ثبت</button>
			</form>
		
		
		</div>

		<div class="col-md-3"></div>
	
	</div>
	
</div>


<?php
	require_once("TAfooter.php");
?>

