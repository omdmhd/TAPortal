<?php
	require_once("header.php");
?>
<?php
	require_once("model/student.php");
	$err = '';
	$success = '';
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		
		if( count($_POST) == 0){
			$err = 'خطا';
		}else{
			if( isset($_POST['password']) && isset($_POST['newpass']) && isset($_POST['reNewPass']) ){
				if($_POST['newpass'] != $_POST['reNewPass']){
					$err = 'پسورد جدید و تکرار آن یکی نیستند ';
				}else{
					$oldPass = $_POST['password'];
					$user = $_SESSION['stuid'];
					$ta = new Student();
					$check = $ta->checkLogin($user,$oldPass);
					if($check['status'] == true){
						$ta->changePass($user,$_POST['newpass']);
						$err = '';
						$success = 'پسورد شما با موفقیت تغییر یافت';
					}else{
						$err = 'پسورد قبلی شما اشتباه است ';
						$success = '';
					}
					
				}
			}else{
				$err = 'تمامی فیلد ها را کامل کنید';
				$success = '';
			}
			
		}
	
	}
?>

<div class="container-fluid" style="margin-top:8%;">
	<div class="row" style="margin-bottom:3%">
		<div class="col-md-12" style="text-align:center;">
			<h1>تغییر پسورد</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6" style="width:2% auto;">


		<form method="POST" >
			  <label class="success"><?php echo $success ?></label>
			  <label class="err" ><?php echo $err;?></label>

			  <div class="form-group">
				<label for="exampleInputEmail1">کلمه عبور قبلی </label>
				<input type="password" name="password" class="form-control" name="startTime" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="کلمه عبور قبلی">
			  </div>

			  <div class="form-group">
				<label for="exampleInputEmail1">کلمه عبور جدید </label>
				<input type="password" name="newpass" class="form-control" name="endTime" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="کلمه عبور جدید">
			  </div>
			  
			  <div class="form-group">
				<label for="exampleInputEmail1">تکرار کلمه عبور جدید </label>
				<input type="password" name="reNewPass" class="form-control" name="endTime" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="تکرار کلمه عبور جدید">
			  </div>




			  <button type="submit" class="btn btn-primary">ثبت</button>
			</form>
		
		
		</div>

		<div class="col-md-3"></div>
	
	</div>
	
</div>


<?php
	require_once("TAfooter.php");
?>

