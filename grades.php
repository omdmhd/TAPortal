<?php 
	require_once("header.php");
?>

<br><br>
<div class="container">
  <h2>نمرات شما در هر درس تا کنون</h2>
  <table class="table">
    <thead>
      <tr>
        <th>نام درس</th>
        <th>نام استاد حل تمرین</th>
        <th>تعداد تمرین های ارایه شده تا کنون</th>
        <th>تعداد تمرین های پاسخ داده شما تا کنون</th>
        <th>نمره شما از حل تمرین تا کنون از 100 نمره</th>
      </tr>
    </thead>
    <tbody>
      <?php
        
        require_once("model/courses.php");
        require_once("model/answers.php");
        
        $crs = new Courses();
        $stu = $_SESSION['stuid'];
        $crses = $crs->getStudentCourse($stu);
        $ans = new Answers();
        foreach( $crses as $crsss) {
            $crsdet = $crs->getCourseDetail($stu,$crsss['course_code']); 
            $data = $ans->gradefunction($stu,$crsss['course_code']);
            echo "
                <tr>
                    <td>".$crsdet[0]["name"]."</td>
                    <td>".$crsdet[0]["taname"]."</td>
                    <td>".$data[0]["count(exercises.name)"]."</td>
                    <td>".$data[0]["count(exercise_answers.id)"]."</td>
                    ";
                if($data[0]["count(exercises.name)"] != 0){
                    echo "<td>".$data[0]["sum(exercise_answers.grade)"]/$data[0]["count(exercises.name)"]."</td></tr>";
                }else{
                    echo "<td>هنوز نمره ای نیست</td></tr>";
                }
        }
        ?>
    </tbody>
  </table>
</div>




<?php
	require_once("footer.php");
	
?>
