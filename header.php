<?php 
	session_start();
	if( isset($_SESSION['stuid'])  && isset($_SESSION['type']) && $_SESSION['type'] == 'student'){
		$username = $_SESSION['username'];
	}else{
		die("ACCESS DENIED");
		
	}

?>
<html>
	<head>
		<link href="static/css/bootstrap.min.css" rel="stylesheet" />
		<link href="static/css/bootstrap-theme.min.css" rel="stylesheet" />
        <script src="static/js/jquery-3.1.1.min.js"></script>
        <script src="static/js/bootstrap.js"></script>
		<link href="static/css/style.css" rel="stylesheet"/>		
	</head>
	<body>
    
	<nav class="navbar navbar-toggleable-md  navbar-light bg-faded ">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="portal.php">پنل دانشجو</a>
      <div class="collapse navbar-collapse" id="navbarCollapse" >
        <ul class="navbar-nav ">
		
          <li class="nav-item">
            <a class="nav-link" href="courses.php">دروس ارایه شده</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="grades.php">مشاهده نمرات</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">ارسال پیام به حل تمرین</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">مشاهده ریز نمرات </a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">پروفایل </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="changePasswd.php">تغییر کلمه ی عبور</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">خروج</a>
          </li>
        </ul>
      </div>
    </nav>
    
    
