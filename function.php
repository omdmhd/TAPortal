<?php

    require_once("model/answers.php");
    require_once("model/courses.php");
    error_reporting(E_ALL);
	ini_set('display_errors', 1);
    if ( isset($_POST['job']) && $_POST['job'] == 'addthiscourse' ){
        $crs = new Courses();
        $crs->addthiscourse($_SESSION['stuid'],$_POST['id']);
    }
    else if ( isset($_POST['job']) && $_POST['job'] == 'livecoursesearch' ){
        $crs = new Courses();
        $live = $crs->livesearch($_POST['text']);
        echo "<table class='table'>
    <thead>
      <tr>
        <th>نام درس</th>
        <th>نام استاد حل تمرین</th>
        <th>لینک عضویت در درس</th>
      </tr>
    </thead>
    <tbody>";
        foreach( $live as $crsss) { 
            echo "
                <tr>
                    <td>".$crsss["cname"]."</td>
                    <td>".$crsss["name"]."</td>
                    <td><button class='btn  btn-block btn-default' role='button' onclick='addthiscourse(".$crsss["id"].")'>درخواست این درس</button ></td>
                </tr>    
                ";
        }
        echo "    
            </tbody>
          </table>
        </div>
        ";
    }
    else if ( isset($_POST['job']) && $_POST['job'] == 'upexe' ){
        $crsid = $_POST['crsid'];
        $exeid = $_POST['exeid'];
        $target_dir = "exercise/".$crsid."/".$exeid."/";
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        session_start();
        $target_file = $target_dir . basename($_FILES["exrfile"]["name"]);
        $uploadOk = 1;
        $FileType = pathinfo($target_file,PATHINFO_EXTENSION);
        if (file_exists($target_file)) {
          echo "این فایل از قبل موجود بوده است";
          $uploadOk = 0;
          }
        else if($_FILES["exrfile"]["name"] != $_SESSION["stuid"].".pdf"){
          $uploadOk = 0;
          echo "توجه داشته باشید نام فایل باید با شماره دانشجویی شما یکی باشد";
        }
          // Check file size
          else if ($_FILES["exrfile"]["size"] > 2000000) {
              echo "حجم فایل شما بیشتر از 2 مگابایت می باشد.";
              $uploadOk = 0;
          }
          // Allow certain file formats
          else if($FileType != "pdf") {
              echo "متاسفانه فرمت فایل شما پی دی اف نیست.";
              $uploadOk = 0;
          }
          // Check if $uploadOk is set to 0 by an error
          else if ($uploadOk == 0) {
              echo "متاسفانه با مشکلی رو به رو شدیم";
          // if everything is ok, try to upload file
          } else {
              if (move_uploaded_file($_FILES["exrfile"]["tmp_name"], $target_file)) {
                    $stu = $_SESSION["stuid"];
                    $ans = new Answers();
                    $status = $ans->add_new_ans($stu,$exeid);
                    if($status == true){
                        echo "تمرین شما با موفقیت ثبت شد";
                    }
                    else{
                          echo "خطای پایگاه داده";
                    }
                }else{
                  echo "متاسفانه فایل نتوانست کپی شود";
              } 
          }
        }

/** Gregorian & Jalali (Hijri_Shamsi,Solar) Date Converter Functions
Author: JDF.SCR.IR =>> Download Full Version : http://jdf.scr.ir/jdf
License: GNU/LGPL _ Open Source & Free _ Version: 2.72 : [2017=1396]
--------------------------------------------------------------------
1461 = 365*4 + 4/4   &  146097 = 365*400 + 400/4 - 400/100 + 400/400
12053 = 365*33 + 32/4    &    36524 = 365*100 + 100/4 - 100/100   */
function dateTimeDiff($date1, $date2) {

    $alt_diff = new stdClass();
    $alt_diff->y =  floor(abs($date1->format('U') - $date2->format('U')) / (60*60*24*365));
    $alt_diff->m =  floor((floor(abs($date1->format('U') - $date2->format('U')) / (60*60*24)) - ($alt_diff->y * 365))/30);
    $alt_diff->d =  floor(floor(abs($date1->format('U') - $date2->format('U')) / (60*60*24)) - ($alt_diff->y * 365) - ($alt_diff->m * 30));
    $alt_diff->h =  floor( floor(abs($date1->format('U') - $date2->format('U')) / (60*60)) - ($alt_diff->y * 365*24) - ($alt_diff->m * 30 * 24 )  - ($alt_diff->d * 24) );
    $alt_diff->i = floor( floor(abs($date1->format('U') - $date2->format('U')) / (60)) - ($alt_diff->y * 365*24*60) - ($alt_diff->m * 30 * 24 *60)  - ($alt_diff->d * 24 * 60) -  ($alt_diff->h * 60) );
    $alt_diff->s =  floor( floor(abs($date1->format('U') - $date2->format('U'))) - ($alt_diff->y * 365*24*60*60) - ($alt_diff->m * 30 * 24 *60*60)  - ($alt_diff->d * 24 * 60*60) -  ($alt_diff->h * 60*60) -  ($alt_diff->i * 60) );
    $alt_diff->invert =  (($date1->format('U') - $date2->format('U')) > 0)? 0 : 1 ;

    return $alt_diff;
} 

function gregorian_to_jalali($gy,$gm,$gd,$mod=''){
 $g_d_m=array(0,31,59,90,120,151,181,212,243,273,304,334);
 if($gy>1600){
  $jy=979;
  $gy-=1600;
 }else{
  $jy=0;
  $gy-=621;
 }
 $gy2=($gm>2)?($gy+1):$gy;
 $days=(365*$gy) +((int)(($gy2+3)/4)) -((int)(($gy2+99)/100)) +((int)(($gy2+399)/400)) -80 +$gd +$g_d_m[$gm-1];
 $jy+=33*((int)($days/12053)); 
 $days%=12053;
 $jy+=4*((int)($days/1461));
 $days%=1461;
 if($days > 365){
  $jy+=(int)(($days-1)/365);
  $days=($days-1)%365;
 }
 $jm=($days < 186)?1+(int)($days/31):7+(int)(($days-186)/30);
 $jd=1+(($days < 186)?($days%31):(($days-186)%30));
 return($mod=='')?array($jy,$jm,$jd):$jy.$mod.$jm.$mod.$jd;
}


function jalali_to_gregorian($jy,$jm,$jd,$mod=''){
 if($jy>979){
  $gy=1600;
  $jy-=979;
 }else{
  $gy=621;
 }
 $days=(365*$jy) +(((int)($jy/33))*8) +((int)((($jy%33)+3)/4)) +78 +$jd +(($jm<7)?($jm-1)*31:(($jm-7)*30)+186);
 $gy+=400*((int)($days/146097));
 $days%=146097;
 if($days > 36524){
  $gy+=100*((int)(--$days/36524));
  $days%=36524;
  if($days >= 365)$days++;
 }
 $gy+=4*((int)($days/1461));
 $days%=1461;
 if($days > 365){
  $gy+=(int)(($days-1)/365);
  $days=($days-1)%365;
 }
 $gd=$days+1;
 foreach(array(0,31,(($gy%4==0 and $gy%100!=0) or ($gy%400==0))?29:28 ,31,30,31,30,31,31,30,31,30,31) as $gm=>$v){
  if($gd<=$v)break;
  $gd-=$v;
 }
 return($mod=='')?array($gy,$gm,$gd):$gy.$mod.$gm.$mod.$gd; 
}

?>
