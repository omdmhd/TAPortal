<?php
	session_start();
	if( isset($_SESSION['username'])  && isset($_SESSION['type']) && $_SESSION['type'] == 'TA'){
		$username = $_SESSION['username'];
	}else{
		//header('Location: index.php');
		//exit();
		die("ACCESS DENIED");
		
	}

?>

<html>
	<head>
		<link href="static/css/bootstrap.min.css" rel="stylesheet" />
		<link href="static/css/bootstrap-theme.min.css" rel="stylesheet" />
		<link href="static/css/style.css" rel="stylesheet"/>		
	</head>
	<body>

	<nav class="navbar navbar-toggleable-md fixed-top navbar-light bg-faded ">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#"><span>سلام  </span><span><?php echo $username;?></span></a>
      <div class="collapse navbar-collapse" id="navbarCollapse" >
        <ul class="navbar-nav ">
		
          <li class="nav-item active">
            <a class="nav-link" href="TAPanel.php">دروس من <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">ارسال پیام به دانشجو</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">صندوق پیام </a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">پروفایل </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="TAChangePasswd.php">تغییر کلمه ی عبور</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">خروج</a>
          </li>
        </ul>
      </div>
    </nav>
    
    
