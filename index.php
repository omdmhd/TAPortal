<!DOCTYPE>
<?php
	session_start();
	require_once("model/student.php");
	require_once("model/ta.php");
	// Start the session

	if( isset($_SESSION['username'])  && isset($_SESSION['type']) && $_SESSION['type'] == 'TA'){
		header('Location: TAPanel.php');
	}else if (isset($_SESSION['stuid'])){
        echo "<script>window.location.replace('portal.php');</script>";
    }
    $username = '';
	$err = '';
	//might find a better way for finding out is it post or get
	if( count($_POST) != 0 ){
		//things should be escaped to prevent sql injection and other bugs
        if(isset($_POST['email'])){
            $err = "در حال ثبت نام";
            $email = $_POST['email'];
            $name = $_POST['name'];
            $userid = $_POST['userRE'];
            $password = $_POST['passRE'];
            $repass = $_POST['passRRE'];
            if($password == $repass){
                $std = new Student();
                $regstatus = $std->add_new_student($userid,$name,$password,$email);
                if($regstatus === true){
                    $err =  "ثبت نام با موفقیت انجام شد";
                }else{
                    $err =  "مشکلی در ثبت نام پیش آماده است";
                }
            }else{
                $err = "رمز و تکرار رمز عبور یکسان نیستند";
            }
            
        }else{

		$username = htmlspecialchars($_POST['username']);
		$password = htmlspecialchars($_POST['password']);
		$std = new Student();
		$out = $std->checkLogin($username,$password);
		if($out['status'] == true && $out['id'] === $username){
            $_SESSION['stuid'] = $username;
			$_SESSION['type'] = 'student';		
			//print_r($out);
			echo "<script>window.location.replace('portal.php');</script>";
			//the user should be redirected and session should be registered
		}else{
			$ta = new TA();
			$out = $ta->checkLogin($username,$password);
			if( $out['status'] === true){
				//the user should be redirected and session should be registered
				$_SESSION['username'] = $out['id'];
				$username =  $out['id'];
				$_SESSION['type'] = 'TA';
				echo "<script>window.location.replace('TAPanel.php');</script>";
			}else{
				$err = 'نام کاربری یا کلمه ی عبور شما اشتباه است';
			}
			
		}
       }
	}
	session_write_close();


?>

<html>
    <head>
        <link href="static/css/style.css" rel="stylesheet" />
        <script src="static/js/jquery-3.1.1.min.js"></script>
        
    </head>
	<body>
        <div class="title">
  <h1>Teaching Assistant Portal (UOK)</h1>
  <span> TA Portal <i class='fa fa-paint-brush'></i> + <i class='fa fa-code'></i> by Azim Atefi And Omid Mohammadi</span>
  <br>
  <span><i class='fa fa-paint-brush'></i>  <i class='fa fa-code'></i><?php echo $err ; ?></span>
</div>
<div class="container">
  <button class="main-button">ورود /  ثبت نام</button>
  <div class="overlayer"></div>
  <div class="popup">
    <div class="login">

      <h2>ورود</h2>
      <form action="index.php" method="POST">
		<div>
			<label class="err"><?php echo $err;?></label>
		</div>
        <div class="input-container">
          <input type="text" name="username" id="username" required="required" value="<?php echo $username?>">
          <label for="username"> شماره دانشجویی</label>
        </div>
        <div class="input-container">
          <input type="password" name="password" id="password" required="required">
          <label for="password" >کلمه عبور</label>
        </div>
        <div class="button-container">
          <button type="submit">ورود</button>
        </div>
        <div class="button-container" style="margin-top:20px">
			<button class="back" href="javascript:void(0)" type="button" aria-hidden="true">ثبت نام</button>          
        </div>
			
				    
        <div class="tips">
          <a href="javascript:void(0)">کلمه عبورتان را فراموش کرده اید؟</a>
        </div>

      </form>
    </div>
    <div class="signup">
	  <h2>ثبت نام</h2>	  
      <form action="index.php" method="POST">
        <div class="input-container">
          <input type="name" id="Name" name="name" required="required">
          <label for="Name">نام و نام خانوادگی</label>
        </div>
        <div class="input-container">
          <input type="email" id="Email" name="email" required="required">
          <label for="Email">پست الکترونیک</label>
        </div>
        <div class="input-container">
          <input type="text" id="UsernameSU" name="userRE" required="required" >
          <label for="UsernameSU"> شماره دانشجویی</label>
        </div>
        <div class="input-container">
          <input type="Password" id="PasswordSU" name="passRE" required="required">
          <label for="PasswordSU">کلمه عبور</label>
        </div>
        <div class="input-container">
          <input type="Password" id="PasswordConfirm" name="passRRE" required="required">
          <label for="PasswordConfirm">تکرار کلمه عبور</label>
        </div>
        <div class="button-container">
          <button>مرحله بعد</button>
        </div>
 
        <div class="button-container" style="margin-top:20px">
			<button class="back" href="javascript:void(0)" type="button" aria-hidden="true">ورود</button>          
        </div>       
        
      </form>
    </div>
  </div>
</div> 
	</body>
</html>

<script>
    $(document).ready(function() {

  var mainBtn = $(".main-button");
  var overlayer = $(".overlayer");
  var popup = $(".popup");
  var toSignUp = $(".login button.back");
  var toLogIn = $(".signup button.back");
  overlayer.fadeIn();
  popup.addClass("open");
  mainBtn.on("click", function() {
    overlayer.fadeIn();
    popup.addClass("open");
  });

  overlayer.on('click', function() {
    $(this).fadeOut();
    if (!popup.hasClass("toSignUp")) {
      popup.removeClass("customAnimation").removeClass("open");
    } else {
      popup.removeClass("customAnimation").removeClass("open").removeClass("toSignUp").addClass("close").delay(700).promise().done(function() {
        $(this).removeClass("close");
      });
      //popup.removeClass("customAnimation").removeClass("open").removeClass("toSignUp");
      //document.querySelector(".popup").setAttribute("class", "popup open");
      //document.querySelector(".popup").offsetWidth;
      //document.querySelector(".popup").setAttribute("class", "popup");
    }
  });

  toSignUp.on('click', function() {
    popup.queue(function(next) {
      popup.addClass("customAnimation");
      next();
    }).queue(function(next) {
      popup.addClass("toSignUp");
      next();
    })
  })

  toLogIn.on('click', function() {
    popup.removeClass("toSignUp");
  });

});
</script>
<style>
    html,
body {
  background: #e9e9e9;
  color: #666666;
  width: 100%;
  height: 100%;
  font-family: 'RobotoDraft', 'Roboto', sans-serif;
  font-size: 14px;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  -webkit-perspective: 500px;
       perspective: 500px;
}


/*Common Title CSS Start*/

div.title {
  padding: 50px 0;
  text-align: center;
  letter-spacing: 2px;
}

div.title h1 {
  margin: 0 0 20px 0;
  font-size: 48px;
  font-weight: 300;
  color: #3e5361;
}

div.title span {
  font-size: 12px;
}

div.title span .fa {
  color: #26C6DA;
}


/*Common Title CSS End*/

* {
  outline: none;
}

div.container {
  text-align: center;
  padding: 20px;
  vertical-align: text-bottom;
}

div.container .main-button {
  margin-top: 50px;
  margin-bottom: 100px;
  padding: 15px;
  width: 200px;
  border: 0;
  border-radius: 3px;
  box-shadow: 1px 5px 20px -5px rgba(0, 0, 0, 0.4);
  color: #fff;
  background-color: #26C6DA;
  cursor: pointer;
  text-align: center;
  letter-spacing: 1px;
  transition: 0.3s ease;
}

div.container .main-button:hover {
  box-shadow: 1px 5px 25px -4px rgba(0, 0, 0, 0.6);
}

div.overlayer {
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.3);
  z-index: 998;
  display: none;
}

div.popup {
  position: fixed;
  width: 520px;
  height: 650px;
  margin: auto;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 999;
  opacity: 0;
  -webkit-transform: scale(0);
  transform: scale(0); /*popup init status*/
  -webkit-transition: all .7s cubic-bezier(0.845, -0.265, 0.190, 1.280);
  transition: all .7s cubic-bezier(0.845, -0.265, 0.190, 1.280);
  -webkit-transform-style: preserve-3d;
       transform-style: preserve-3d;
}

div.popup.open {
  opacity: 1;
  -webkit-transform: scale(1);
  transform: scale(1); /*popup final status*/
}

div.popup.customAnimation {
  -webkit-transition: all .5s;
  transition: all .5s;
}

div.popup.toSignUp {
  -webkit-transform: scale(1) rotateY(180deg);
  transform: scale(1) rotateY(180deg);
}

div.popup.close {
  opacity: 0;
  -webkit-transform: scale(0) rotateY(180deg);
  transform: scale(0) rotateY(180deg); /*popup init status*/
}

div.login, div.signup {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: #fff;
  box-shadow: 1px 10px 30px -10px rgba(0, 0, 0, 0.5);
  -webkit-backface-visibility: hidden;
       backface-visibility: hidden;
  z-index: 1000;
}

div.login {
  z-index: 2;
  -webkit-transform: rotateY(0deg);
       transform: rotateY(0deg);
}

div.signup {
  -webkit-transform: rotateY(180deg);
       transform: rotateY(180deg);  
}

.login a.back {
  color: #fff;
  float: right;
  font-size: 18px;
  padding: 10px;
}

.signup a.back {
  color: #fff;
  text-align:center;

  font-size: 18px;
  padding: 10px;
}

.login h2 {
  margin-top: 0;
  margin-bottom: 20px;
  padding-top: 110px;
  padding-bottom: 20px;
  font-size: 22px;
  font-weight: 400;
  color: #fff;
  background-color: #26C6DA;
}

.signup h2 {
  margin-top: 0;
  margin-bottom: 20px;
  padding-top: 22px;
  padding-bottom: 20px;
  font-size: 22px;
  font-weight: 400;
  color: #fff;
  background-color: #ef5350;
}

form {
  display: block;
}

.input-container {
  position: relative;
  margin: 0 20px 20px;
}

.input-container input {
  padding: 12px 5px;
  width: 262px;
  border: 1px solid #9d9d9d;
  font-size: 20px;
  font-weight: 400;
     direction: rtl;
  color: #212121;
}

.input-container label {
  position: absolute;
  padding: 0 2px;
  top: 0;
  right: 120px;
  color: #AEAEAE;
  font-size: 20px;
  font-weight: 300;
  line-height: 50px;
     direction: rtl;
  cursor: text;
  -webkit-transition: 0.2s ease;
  transition: 0.2s ease;
  /**/
}

.input-container input:focus ~ label {
  color: #26C6DA;
  line-height: 17px;
  background-color: #fff;
  -webkit-transform: translate(-12%, -50%) scale(0.75);
  transform: translate(-12%, -50%) scale(0.75);
}

.signup .input-container input:focus ~ label {
  color: #ef5350;
}

.input-container input:valid ~ label {
  color: #26C6DA;
  line-height: 17px;
  background-color: #fff;
  -webkit-transform: translate(-12%, -50%) scale(0.75);
  transform: translate(-12%, -50%) scale(0.75);
}

.signup .input-container input:valid ~ label {
  color: #ef5350;
}

.button-container button {
  border: none;
  padding: 10px;
  width: 200px;
  color: #fff;
  background-color: #26C6DA;
  border-radius: 3px;
  box-shadow: 1px 5px 20px -5px rgba(0, 0, 0, 0.4);
}

.signup .button-container button {
  background-color: #ef5350;
}

.tips {
  margin-top: 14px;
}

.tips a{
  text-decoration: none;
  color: #b3b3b3;
  font-size: 12px;
}


</style>
