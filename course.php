<?php 
    require_once("function.php");
    require_once("model/exercise.php");
	require_once("header.php");
?>
<br><br>
<div class="container">
  <h2>تمرین های این درس</h2>
  <table class="table">
    <thead>
      <tr>
        <th>نام تمرین</th>
        <th>زمان شروع تمرین</th>
        <th>زمان خاتمه تمرین</th>
        <th>وضعیت تمرین</th>
        <th>نمره کسب شده</th>
      </tr>
    </thead>
    <tbody>
      <?php     

        $stu = $_SESSION['stuid'];
        $crsid = $_GET['id'];
        $exe = new Exercise();
        $crsexe = $exe->ShowExercises($crsid);
        foreach( $crsexe as $crsss) {
            $st =  explode("-",$crsss["start_date"]);
            $stp = gregorian_to_jalali($st[0],$st[1],$st[2]);
            $en =  explode("-",$crsss["end_date"]);
            $enp = gregorian_to_jalali($en[0],$en[1],$en[2]); 
            echo "
                <tr>
                    <td>".$crsss["name"]."</td>
                    <td>".$stp[0]."-".$stp[1]."-".$stp[2]."</td>
                    <td>".$enp[0]."-".$enp[1]."-".$enp[2]."</td>
                    ";
                    $end = new DateTime($crsss["end_date"]);
                    $today = new DateTime(date("Y-m-d"));
                    $comp = dateTimeDiff($today,$end);//$today->diff($end);
                    $check = $exe->CheckExerSolve($stu,$crsss["id"]);
                    if ($check[0] == true){
                        echo "<td><a href='course.php?id=".$crsss["id"]."' class='btn  btn-block btn-default disabled' role='button'>شما به این تمرین پاسخ داده اید</a></td>
                        ";
                        if($check[1] != -1){
                            echo "<td>".$check[1]."</td>";
                        }
                        else{
                            echo "<td>نمره شما هنوز ثبت نشده است</td>";
                        }
                    }
                    else if($comp->invert == 1){
                        echo"<td><button  class='btn  btn-block btn-default ' role='button' type='button' data-toggle='modal' data-target='#".$crsss["id"]."' >آپلود تمرین</button></td>
                        <td>هنوز تمرین را ثبت نکرده اید</td>
                        ";
                        echo '
                            <div class="modal fade" id="'.$crsss["id"].'" role="dialog">
                                <div class="modal-dialog modal-sm">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">فرم آپلود تمرین</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="file" name = "exefile" >
                                        <div class="progress" style="display:none;">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">

                                          </div>
                                        </div>
                                        
                                        <div id="errorcls'.$crsss["id"].'"></div>
                                    </div>
                                    <div class="modal-footer">
                                      <button class="btn btn-default btn-block" onclick="uploadexr('.$crsss["id"].','.$crsid.')">تایید</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            ';
                    }else{
                        echo"<td><a href='course.php?id=".$crsss["id"]."' class='btn  btn-block btn-default disabled' role='button'>زمان تمرین به پایان رسیده است</a></td>
                        <td> نمره شما صفر در نظر گرفته می شود</td>
                        ";
                    }
                echo  "
                </tr>
                ";
        }
        ?>
    </tbody>
  </table>
</div>
<script>
    function uploadexr(id,crsid){
        formData = new FormData();
        formData.append('job', 'upexe');
        formData.append('exeid', id);
        formData.append('crsid', crsid)
        // Attach file
        formData.append('exrfile', $('input[type=file]')[0].files[0]); 
        $('.progress').show();
        $.ajax({
            xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.progress-bar').css('width',percentComplete+"%");
                            $('.progress-bar').html(percentComplete+"%");
                            if (percentComplete === 100) {

                        }
                      }
                    }, false);
                    return xhr;
                  },
            url: "function.php", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false, 
            cache:false,
            success: function(data)   // A function to be called if request succeeds
                {
                    $('.progress').hide(); document.getElementById('errorcls'+id).innerHTML=data 
                }
        });
    }

</script>
<?php
	require_once("footer.php");
	
?>
