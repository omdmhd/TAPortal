CREATE DATABASE TAPortal;
connect TAPortal;
CREATE TABLE students(
	id varchar(15) Primary Key,
	name varchar(50),
	password varchar(20),
	email varchar(100)
);


CREATE TABLE courses(
	id int Primary Key AUTO_INCREMENT,
	name varchar(100)
);

CREATE TABLE course_student(
	id int Primary Key AUTO_INCREMENT,
	course_code int,
	student_code varchar(15),
	confirm Boolean,
	FOREIGN KEY (course_code) REFERENCES courses(id),
	FOREIGN KEY (student_code) REFERENCES students(id)
);

CREATE TABLE TA_users(
	username varchar(50) Primary Key,
	name varchar(100),
	password varchar(50)
);

CREATE TABLE TA_courses(
	id int Primary Key AUTO_INCREMENT,
	TA varchar(50),
	course_code int,
	FOREIGN KEY (TA) REFERENCES TA_users(username),
	FOREIGN KEY (course_code) REFERENCES courses(id)

);

CREATE TABLE exercises(
	id int Primary Key AUTO_INCREMENT,
	name varchar(100),
	description varchar(5000),
	start_date DATETIME,
	end_date DATETIME,
	course_code int,
	file_name varchar(100),
	expire_message varchar(6000),
	FOREIGN KEY (course_code) REFERENCES courses(id)

);

CREATE TABLE exercise_answers(
	id int Primary Key AUTO_INCREMENT,
	student varchar(15),
	exercise int,
	file_name varchar(100),
	grade int Default 0,
	FOREIGN KEY (student) REFERENCES students(id),
	FOREIGN KEY (exercise) REFERENCES exercises(id)
);




